package vp.spring.furniture.web.dto;

import java.util.ArrayList;
import java.util.List;

import vp.spring.furniture.model.TypeOfFurniture;

public class TypeOfFurnitureDTO {

	private Long id;
	private String name;
	private List<FurnitureDTO> furnitures = new ArrayList<FurnitureDTO>();

	public TypeOfFurnitureDTO() {
	}

	public TypeOfFurnitureDTO(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public TypeOfFurnitureDTO(TypeOfFurniture typeOfFurniture) {
		this.id = typeOfFurniture.getId();
		this.name = typeOfFurniture.getName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<FurnitureDTO> getFurnitures() {
		return furnitures;
	}

	public void setFurnitures(List<FurnitureDTO> furnitures) {
		this.furnitures = furnitures;
	}

}
