package vp.spring.furniture.web.dto;

import vp.spring.furniture.model.Furniture;

public class FurnitureDTO {

	private Long id;
	private String name;
	private String password;
	private double price;
	private TypeOfFurnitureDTO typeOfFurniture;

	public FurnitureDTO(Furniture furniture) {
		this.id = furniture.getId();
		this.name = furniture.getName();
		this.password = furniture.getPassword();
		this.price = furniture.getPrice();
	}
	
	public FurnitureDTO(Long id, String name, String password, double price) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.price = price;
	}
	

	public FurnitureDTO() {
		super();
	}

	public TypeOfFurnitureDTO getTypeOfFurniture() {
		return typeOfFurniture;
	}

	public void setTypeOfFurniture(TypeOfFurnitureDTO typeOfFurniture) {
		this.typeOfFurniture = typeOfFurniture;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
