package vp.spring.furniture.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.furniture.model.Furniture;
import vp.spring.furniture.service.FurnitureService;
import vp.spring.furniture.web.dto.FurnitureDTO;

@RestController
public class FurnitureController {
	@Autowired
	FurnitureService furnitureService;
	
	@RequestMapping(value = "api/bla", method = RequestMethod.GET)
	public ResponseEntity<List<FurnitureDTO>> getAllFurniture(Pageable page){
		Page<Furniture> furniture = furnitureService.findAll(page);
		List<FurnitureDTO> retVal = convertToDTO(furniture.getContent());
		return new ResponseEntity<List<FurnitureDTO>>(retVal, HttpStatus.OK);
		}
		
	
	private List<FurnitureDTO> convertToDTO(List<Furniture> furnitures){
		List<FurnitureDTO>retVal = new ArrayList<>();
		for(Furniture furniture: furnitures) {
			FurnitureDTO furnitureDTO = new FurnitureDTO(furniture);
			retVal.add(furnitureDTO);
		}
		return retVal;
	}
	
	@RequestMapping(value = "api/furniture", method = RequestMethod.GET, params = "password")
	public ResponseEntity<List<FurnitureDTO>> getFurniture(@RequestParam String password){
		List<Furniture> furnitures = furnitureService.findByPass(password);
		List<FurnitureDTO> retVal = new ArrayList<>();
		for(Furniture furniture: furnitures) {
			FurnitureDTO furnitureDTO = new FurnitureDTO(furniture);
			retVal.add(furnitureDTO);
		}
		return new ResponseEntity<List<FurnitureDTO>>(retVal, HttpStatus.OK);
	}

	@RequestMapping(value = "api/furnitures", method = RequestMethod.GET, params = {"price1","price2"})
	public ResponseEntity<List<FurnitureDTO>> getFurniturePriceBeetwen(@RequestParam double price1, @RequestParam double price2){
		List<Furniture> furnitures = furnitureService.findByPrice(price1, price2);
		List<FurnitureDTO> retVal = new ArrayList<>();
		for(Furniture furniture: furnitures) {
			FurnitureDTO furnitureDTO = new FurnitureDTO(furniture);
			retVal.add(furnitureDTO);
		}
		return new ResponseEntity<List<FurnitureDTO>>(retVal, HttpStatus.OK);
	}
	

}
