package vp.spring.furniture.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.spring.furniture.model.Furniture;
import vp.spring.furniture.model.TypeOfFurniture;
import vp.spring.furniture.service.TypeOfFurnitureService;
import vp.spring.furniture.web.dto.FurnitureDTO;
import vp.spring.furniture.web.dto.TypeOfFurnitureDTO;

@RestController
public class TypeOfFurnitureController {
	@Autowired
	TypeOfFurnitureService typeOfFurnitureService;
	
	@RequestMapping(value = "api/typeoffurniture/{id}", method = RequestMethod.GET)
	public ResponseEntity<TypeOfFurnitureDTO> getTypeById(@PathVariable Long id){
		TypeOfFurniture type = typeOfFurnitureService.findOne(id);
		if( type != null) {
			TypeOfFurnitureDTO typeDTO = new TypeOfFurnitureDTO(type);
			for(Furniture furniture: type.getFurnitures()) {
				typeDTO.getFurnitures().add(new FurnitureDTO(furniture));
			}
			return new ResponseEntity<TypeOfFurnitureDTO>(typeDTO, HttpStatus.OK);
		}else {
			return new ResponseEntity<TypeOfFurnitureDTO>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "api/furnituretypes", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TypeOfFurnitureDTO> create(@RequestBody TypeOfFurnitureDTO typeOfFurnitureDTO){
		TypeOfFurniture typeOfFurniture = new TypeOfFurniture();
		typeOfFurniture.setName(typeOfFurnitureDTO.getName());
		typeOfFurniture = typeOfFurnitureService.save(typeOfFurniture);
		return new ResponseEntity<>(new TypeOfFurnitureDTO(typeOfFurniture), HttpStatus.CREATED);
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "api/furniturestypes/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TypeOfFurnitureDTO>update(@RequestBody TypeOfFurnitureDTO typeOfFurnitureDTO, @PathVariable Long id){
		TypeOfFurniture typeOfFurniture = typeOfFurnitureService.findOne(id);
		typeOfFurniture.setId(id);
		typeOfFurniture.setName(typeOfFurnitureDTO.getName());
		typeOfFurniture = typeOfFurnitureService.save(typeOfFurniture);
		TypeOfFurnitureDTO typeDTO = new TypeOfFurnitureDTO(typeOfFurniture);
		return new ResponseEntity<TypeOfFurnitureDTO>(typeDTO, HttpStatus.OK);
		
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "api/furnituretypes/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Long id){
		TypeOfFurniture type = typeOfFurnitureService.findOne(id);
		if (type != null) {
			typeOfFurnitureService.remove(id);
			return new ResponseEntity<>(HttpStatus.OK);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "api/furnituretypes", method = RequestMethod.GET)
	public ResponseEntity<List<TypeOfFurnitureDTO>> getTypeFurniturePage(Pageable page) {
		Page<TypeOfFurniture> types = typeOfFurnitureService.findAll(page);
		List<TypeOfFurnitureDTO> retVal = convert(types.getContent());
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	
	@RequestMapping(value = "api/furnituretypes", method = RequestMethod.GET,params = "name")
	public ResponseEntity<List<TypeOfFurnitureDTO>> getByName(@RequestParam String name){
		List<TypeOfFurniture> types = typeOfFurnitureService.findByName(name);
		List<TypeOfFurnitureDTO> retVal = new ArrayList<>();
		for (TypeOfFurniture typeOfFurniture : types) {
			TypeOfFurnitureDTO typeDTO = new TypeOfFurnitureDTO(typeOfFurniture);
			retVal.add(typeDTO);
		}
		return new ResponseEntity<>(retVal, HttpStatus.OK);
	}
	@RequestMapping(value = "api/furnituretypes", method = RequestMethod.GET, params = "price1")
	public ResponseEntity<List<TypeOfFurnitureDTO>> getFurnitureByNameAndPrice(@RequestParam Double price1){
		List<TypeOfFurniture> fornituresType = typeOfFurnitureService.findByTypeAndPrice(price1);
		List<TypeOfFurnitureDTO> retVal = new ArrayList<>();
		for (TypeOfFurniture furniture: fornituresType) {
			TypeOfFurnitureDTO furnitureDTO = new TypeOfFurnitureDTO(furniture);
			retVal.add(furnitureDTO);
		}
		return new ResponseEntity<List<TypeOfFurnitureDTO>>(retVal, HttpStatus.OK);
	}
	
	private List<TypeOfFurnitureDTO> convert(List<TypeOfFurniture> types) {
		List<TypeOfFurnitureDTO> retVal = new ArrayList<TypeOfFurnitureDTO>();
		for (TypeOfFurniture typeOfFurniture : types) {
			TypeOfFurnitureDTO typeOfFurnitureDTO = new TypeOfFurnitureDTO(typeOfFurniture);
			for (Furniture furniture: typeOfFurniture.getFurnitures()) {
				typeOfFurnitureDTO.getFurnitures().add(new FurnitureDTO(furniture));
			}
			retVal.add(typeOfFurnitureDTO);
		}
	return retVal;
	}
}