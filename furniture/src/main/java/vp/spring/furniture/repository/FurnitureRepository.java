package vp.spring.furniture.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import vp.spring.furniture.model.Furniture;

@Component
public interface FurnitureRepository extends JpaRepository<Furniture, Long> {
	
	public List<Furniture> findByPassword(String pass);
	
	@Query("select f from Furniture f where f.price > :price1 and f.price< :price2")
	public List<Furniture> findByPrice(@Param("price1")double price1,@Param("price2")double price2);


}
