package vp.spring.furniture.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import vp.spring.furniture.model.Furniture;
import vp.spring.furniture.model.TypeOfFurniture;

@Component
public interface TypeOfFurnitureRepository extends JpaRepository<TypeOfFurniture, Long> {

	public List<TypeOfFurniture> findByNameContains(String name);
	
	
	@Query("select distinct t from TypeOfFurniture t left join fetch t.furnitures f where f.price>:price1")
	public List<TypeOfFurniture> findByTypeAndPrice(@Param("price1") Double price1);
	
}
