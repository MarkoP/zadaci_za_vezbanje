package vp.spring.furniture.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.spring.furniture.model.TypeOfFurniture;
import vp.spring.furniture.repository.TypeOfFurnitureRepository;

@Component
public class TypeOfFurnitureService {
	@Autowired
	TypeOfFurnitureRepository typeOfFurnitureRepository;
	
	
	public Page<TypeOfFurniture> findAll(Pageable page){
		return typeOfFurnitureRepository.findAll(page);
	}
	
	public List<TypeOfFurniture>findByName(String name){
		return typeOfFurnitureRepository.findByNameContains(name);
	}
	
	public TypeOfFurniture findOne(Long id) {
		return typeOfFurnitureRepository.findOne(id);
	}
	
	public TypeOfFurniture save(TypeOfFurniture typeOfFurniture) {
		return typeOfFurnitureRepository.save(typeOfFurniture);
	}
	public void remove(Long id) {
		typeOfFurnitureRepository.delete(id);
	}
	public List<TypeOfFurniture> findByTypeAndPrice(Double price1){
		return typeOfFurnitureRepository.findByTypeAndPrice(price1);
	}
	
}
