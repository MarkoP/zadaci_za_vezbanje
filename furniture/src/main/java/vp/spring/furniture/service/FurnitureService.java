package vp.spring.furniture.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.spring.furniture.model.Furniture;
import vp.spring.furniture.repository.FurnitureRepository;

@Component
public class FurnitureService {
	@Autowired
	FurnitureRepository furnitureRepository;
	
	public List<Furniture> findByPass(String password){
		return furnitureRepository.findByPassword(password);
	}
	
	public List<Furniture> findByPrice(double price1, double price2){
		return furnitureRepository.findByPrice(price1, price2);
	}
	
	public Page<Furniture> findAll(Pageable page){
		return furnitureRepository.findAll(page);
	}
}
