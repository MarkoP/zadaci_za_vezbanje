package vp.spring.furniture.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Furniture {

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private String password;
	private double price;

	@ManyToOne(fetch = FetchType.EAGER)
	private TypeOfFurniture type;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}


	public TypeOfFurniture getType() {
		return type;
	}

	public void setType(TypeOfFurniture type) {
		this.type = type;
	}

	public Furniture(Long id, String name, String password, double price) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.price = price;
	}

	public Furniture() {
	}

}
