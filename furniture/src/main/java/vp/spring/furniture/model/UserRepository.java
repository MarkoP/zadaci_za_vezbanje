package vp.spring.furniture.model;

import org.springframework.data.jpa.repository.JpaRepository;

import vp.spring.furniture.model.user.SecurityUser;



public interface UserRepository extends JpaRepository<SecurityUser, Long> {
	  public SecurityUser findByUsername(String username);
}
