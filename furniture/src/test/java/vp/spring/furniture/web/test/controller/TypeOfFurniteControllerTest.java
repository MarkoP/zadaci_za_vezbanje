package vp.spring.furniture.web.test.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import vp.spring.furniture.service.TypeOfFurnitureService;
import vp.spring.furniture.web.dto.TypeOfFurnitureDTO;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class TypeOfFurniteControllerTest {

	@Autowired
	private TestRestTemplate restTemplate;
	
	@Autowired
	private TypeOfFurnitureService typeOfFurnitureService;
	
	@Test
	public void testirenjaTipaNamestajaPoImenu() {
		ResponseEntity<TypeOfFurnitureDTO[]> responseEntity =
				restTemplate.getForEntity("api/furnituretypes", TypeOfFurnitureDTO[].class);
		TypeOfFurnitureDTO[] typeOfFurnitures = responseEntity.getBody();
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(1, typeOfFurnitures.length);
		assertEquals("Sto", typeOfFurnitures[0].getName());
	}
}
