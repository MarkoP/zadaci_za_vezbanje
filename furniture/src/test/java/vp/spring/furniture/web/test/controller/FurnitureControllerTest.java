package vp.spring.furniture.web.test.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import vp.spring.furniture.service.FurnitureService;
import vp.spring.furniture.web.dto.FurnitureDTO;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class FurnitureControllerTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private FurnitureService furnitureService;

	@Test
	public void testiranjeNamestaja() {
		ResponseEntity<FurnitureDTO[]> responseEntity = restTemplate.getForEntity("/api/bla?page=0&size=2",
				FurnitureDTO[].class);

		FurnitureDTO[] furnitures = responseEntity.getBody();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(2, furnitures.length);
		assertEquals("Drveni sto", furnitures[0].getName());
		assertEquals("Plasticni sto", furnitures[1].getName());
	}

	@Test
	public void testiranjeNamestaPoSifri() {
		ResponseEntity<FurnitureDTO[]> responseEntity = restTemplate.getForEntity("/api/furniture?password=V10",
				FurnitureDTO[].class);

		FurnitureDTO[] furnitures = responseEntity.getBody();

		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(1, furnitures.length);
		assertEquals("V10", furnitures[0].getPassword());
	}
	
	@Test
	public void testiranjeCenovnogOpsega() {
		ResponseEntity<FurnitureDTO[]> responseEntity = restTemplate.getForEntity("/api/furnitures?price1=1499&price2=21000",
				FurnitureDTO[].class);

		FurnitureDTO[] furnitures = responseEntity.getBody();
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(4, furnitures.length);
		assertEquals("Valentina", furnitures[1].getName());
		assertEquals("Plasticni sto", furnitures[3].getName());
	}
	@Test
	public void testiranjeNamestajaPoImenuIcenomVecomOd() {
		ResponseEntity<FurnitureDTO[]> responseEntity = restTemplate.getForEntity("/api/furnitures?price1=1499&name=Sto",
				FurnitureDTO[].class);

		FurnitureDTO[] furnitures = responseEntity.getBody();
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		
	}
	
	@Test
	public void testiranjeTipaNamestajaSkupljegOdOdredjeneCene() {
		ResponseEntity<FurnitureDTO[]> responseEntity = restTemplate.getForEntity("/api/furnitures?price1=1499&price2=21000",
				FurnitureDTO[].class);
		
		FurnitureDTO[] furnitures = responseEntity.getBody();
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals(4, furnitures.length);
		assertEquals("Plasticni sto", furnitures[0].getName());
		assertEquals("V10", furnitures[1].getPassword());
		assertEquals(1500, 1500, furnitures[2].getPrice());
		assertEquals(7,7, furnitures[3].getId());
	}
}
