package vp.spring.furniture.test.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import vp.spring.furniture.model.Furniture;
import vp.spring.furniture.repository.FurnitureRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class FurnitureTest {

	@Autowired
	FurnitureRepository furnitoreRepository;
	
	@Test
	public void testFindByPassword() {
		List<Furniture> furnitures = furnitoreRepository.findByPassword("DS100");
		
		assertEquals("Drveni sto", furnitures.get(0).getName());
		assertEquals(21000, 21000);
	}
	@Test
	public void testFindByPrice() {
		List<Furniture> furnitures = furnitoreRepository.findByPrice(1500, 15000);
		assertEquals(2, furnitures.size());
		
	}
/*	@Test
	public void test1() {
		List<Furniture> furnitures = furnitoreRepository.findByTypeAndPrice(1500);
		
		assertEquals(1,furnitures.size());
		assertEquals("Valentina",furnitures.get(0).getName());
		assertEquals(Long.valueOf(3), furnitures.get(0).getId());
		assertNotNull(furnitures.get(0));
		}*/
}
