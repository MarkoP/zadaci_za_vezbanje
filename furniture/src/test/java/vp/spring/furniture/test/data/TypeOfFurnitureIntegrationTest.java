package vp.spring.furniture.test.data;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import vp.spring.furniture.model.TypeOfFurniture;
import vp.spring.furniture.repository.TypeOfFurnitureRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class TypeOfFurnitureIntegrationTest {
	
	@Autowired
	TypeOfFurnitureRepository typeOfFurnitureRepository;
	
	@Test
	public void testFindByNameContains() {
		//List<TypeOfFurniture> types = typeOfFurnitureRepository.findByNameContains("stolica");
		List<TypeOfFurniture> types = typeOfFurnitureRepository.findAll();
		
		assertEquals(6, types.size());
		assertEquals("Krevet", types.get(2).getName());
		assertEquals(Long.valueOf(3), types.get(2).getId());
		//assertEquals(ovde pisemo rezultat koji ocekujemo da vrati parametar sa 
		//desne strane, ove ide parametar koji treba da vrati rezultat koji je sa leve strnae);
		//sto znaci da trazimo da li postoji objekat u listi types, sa nazivom stolica
		//ovo proverava, pristupi mi listi types pod indeksom nula proveri da li postoji ime stolica
		//assertEquals("Stolica", types.get(0).getName());
	}
}
